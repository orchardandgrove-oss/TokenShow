//
//  Extensions.swift
//  TokenShow
//
//  Created by Joel Rennich on 12/10/17.
//  Copyright © 2017 Joel Rennich. All rights reserved.
//

import Foundation
import Cocoa

extension ViewController : NSTableViewDataSource {
    
    func numberOfRows( in tableView: NSTableView ) -> Int {
        return identities?.count ?? 0
    }
}

extension Data {
    func hexEncodedString() -> String {
        return map { String(format: "%02hhx", $0) }.joined()
    }
}

extension String {
    func printBits() -> String {
        var output = ""
        for i in 0...(self.count - 1) {
            output.append(self[self.index(self.startIndex, offsetBy: i)])
            
            if i == 0 || i % 2 == 0 || i == (self.count - 1) {
                continue
            } else {
                output += ", "
            }
        }
        return output
    }
}

extension ViewController : NSTableViewDelegate {
    
    fileprivate enum CellIDs {
        static let cn = "cn-list"
        static let pkh = "pkh-list"
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int ) -> NSView? {
        
        var text = ""
        var cellIdentifier: String = ""
        
        guard let item = identities?[row] else {
            return nil
        }
        
        if tableColumn == tableView.tableColumns[0] {
            text = item.cn
            cellIdentifier = CellIDs.cn
        } else if tableColumn == tableView.tableColumns[1] {
            text = item.pubKeyHash
            cellIdentifier = CellIDs.pkh
        }
        
        if let cell = tableView.makeView(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: cellIdentifier), owner: nil) as? NSTableCellView {
            cell.textField?.stringValue = text
            return cell
        }
        return nil
    }
}
