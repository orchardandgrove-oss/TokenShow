//
//  Keychainutil.swift
//  CertPickerUI
//
//  Created by Joel Rennich on 12/3/16.
//  Copyright © 2016 Trusource Labs. All rights reserved.
//

import Foundation

// class to do all the keychain work

enum keychainType {
    static let standard = 0
    static let token = 1
    static let card = 2
}

struct identityList {
    var cn: String
    var pubKeyHash: String
    var identity: SecIdentity
    var oids: String
    var token: Bool
    var keychain: Int
    var keychainRef : SecKeychain?
    var principal : String?
    var friendlyName: String?
    var ca: String?
    var caOrg: String?
}

// bitwise convenience
prefix operator ~~

prefix func ~~(value: Int) -> Bool {
    return (value > 0) ? true : false
}

class KeychainUtil {
    
    var myErr: OSStatus
    
    var cnString = ""
    
    init() {
        myErr = 0
    }
    
    // **** CHANGES HERE ****
    
    var matchValue = "UPN"
    
    var debug = false
    
    var caMatch = ""
    var caOrgMatch = ""
    
    var filterOID = ""
    var filterTerm = ""
    
    // **** CHANGES DONE
    
    func findIdentities(tokensOnly: Int, filter: Bool=false) -> [identityList] {
        
        var searchReturn: AnyObject? = nil
        var idList = [identityList]()
        var myCert: SecCertificate? = nil
        
        var friendlyName: String? = nil
        
        // we need to loop through a) CTK-based identities, then b) each keychain to find out which identities go to which keychains
        
        // 1. find all CTK-based identities - this only works on 10.12+
        
        if #available(OSX 10.12, *) {
            let tokenSearchDict: [String:AnyObject] = [
                kSecAttrAccessGroup as String:  kSecAttrAccessGroupToken,
                kSecClass as String: kSecClassIdentity,
                kSecReturnAttributes as String: true as AnyObject,
                kSecReturnRef as String: true as AnyObject,
                kSecMatchLimit as String : kSecMatchLimitAll as AnyObject
            ]
            myErr = SecItemCopyMatching(tokenSearchDict as CFDictionary, &searchReturn)
            
            if myErr != 0 {
                //NSLog("Error finding identities.")
                // break or return
            }
            
            // check to see we have results
            
            if searchReturn != nil {
                
                // we have results to loop through them and add them
                
                let foundIdentites = searchReturn as! CFArray as Array
                
                for identity in foundIdentites {
                    
                    var certAuthority: String? = nil
                    var certOrg: String? = nil
                    
                    // get the OIDs
                    myErr = SecIdentityCopyCertificate(identity["v_Ref"] as! SecIdentity, &myCert)
                    
                    if myErr != 0 {
                        // NSLog("Error get SecCertificate from a SecIdentity")
                        // break or return
                        continue
                    }
                    
                    let myPrincipal = getPrincipal(identity: myCert!)
                    
                    let myOIDs : NSDictionary = SecCertificateCopyValues(myCert!, nil, nil)!
                    
                    // filter on OIDS
                    
                    if myOIDs["2.5.29.15"] != nil {
                        //                        guard let usage = myOIDs["2.5.29.15"] as? NSDictionary else { continue }
                        //                        guard let usageValue = usage["value"] as? Int else { continue }
                        //
                        //                        if ~~( abs(usageValue) & 128 ) { //&& defaults.bool(forKey: "ShowPIVAuthentication") {
                        //                            friendlyName = "PIV Authentication"
                        //                        } else if ~~( abs(usageValue) & 192 ) && defaults.bool(forKey: "ShowPIVSignature") {
                        //                            friendlyName = "PIV Signature"
                        //                        } else if ~~( abs(usageValue) & 32 ) && defaults.bool(forKey: "ShowKeyEncipherment") {
                        //                            friendlyName = "Key Encipherment"
                        //                        } else {
                        //                            friendlyName = "Other Cert"
                        //                            if !defaults.bool(forKey: "ShowAll") {
                        //                                continue
                        //                            }
                        //                        }
                        
                    } else {
                        //print("NOT A KEY")
                        continue
                    }
                    
                    // get the CN from the SecCertificate
                    var myCN: CFString? = nil
                    myErr = SecCertificateCopyCommonName(myCert!, &myCN)
                    
                    // skip this loop if there's no label
                    if identity["labl"] == nil {
                        NSLog("labl is empty")
                        cnString = "No label"
                    } else {
                        cnString = identity["labl"] as! String
                    }
                    
                    // find CA
                    
                    if myOIDs["2.16.840.1.113741.2.1.1.1.5"] != nil {
                        guard let certAuthorityDict = myOIDs["2.16.840.1.113741.2.1.1.1.5"] as? [String:AnyObject] else { continue }
                        guard let certAuthorityDictValues = certAuthorityDict["value"] as? [[String:AnyObject]] else { continue }
                        for item in certAuthorityDictValues {
                            if item["label"] as! String == "2.5.4.3" {
                                certAuthority = ((item["value"] as? String) ?? nil)!
                            } else if item["label"] as! String == "2.5.4.11" {
                                certOrg = ((item["value"] as? String) ?? nil)!
                            }
                        }
                    }
                    
                    // filter on CA Org
                    
                    // get the pubkey hash
                    let certPubKeyHash = identity["pkhh"] as? Data
                    let certPubKeyHashString = certPubKeyHash?.hexEncodedString().uppercased()
                    
                    if caMatch != "" && certAuthority != caMatch {
                        // skipping identity b/c of no CA match
                        continue
                    }
                    
                    if caOrgMatch != "" && certOrg != caOrgMatch {
                        // skipping identity b/c of no CA Org match
                        continue
                    }
                    
                    if filterOID != "" {
                        guard let filterItem = myOIDs[filterOID] as? [String:AnyObject] else {
                            continue
                        }
                        
                        if filterItem["value"] as? String != filterTerm {
                            // no match
                            
                            continue
                        }
                    }
                    
                    //NSLog("Adding Identity: " + cnString + ":" + certPubKeyHashString!)
                    let myidList = identityList(cn: cnString, pubKeyHash: certPubKeyHashString!, identity: identity["v_Ref"] as! SecIdentity, oids: myOIDs.description, token: true, keychain: keychainType.token, keychainRef: nil, principal: myPrincipal, friendlyName: friendlyName, ca: nil, caOrg: certOrg)
                    idList.append(myidList)
                }
            }
        }
        
        // if we're only looking for tokens we don't need to go any further
        
        if tokensOnly == 1 {
            return idList
        }
        
        // 2. find all the keychains
        
        var keychainList: CFArray? = nil
        
        myErr = SecKeychainCopySearchList(&keychainList)
        
        // 3. loop through the keychain list and find the identities in each one
        
        var cardFlag = false
        
        for keychain in keychainList as! Array<SecKeychain> {
            
            // check if we're a keychain or a card
            
            var pathLen = UInt32(PATH_MAX)
            var pathName: [Int8] = [Int8](repeating: 0, count: Int(PATH_MAX))
            
            myErr = SecKeychainGetPath(keychain, &pathLen, &pathName)
            // print(URL(fileURLWithFileSystemRepresentation: pathName, isDirectory: false, relativeTo: nil))
            
            if String(describing: URL(fileURLWithFileSystemRepresentation: pathName, isDirectory: false, relativeTo: nil)).hasPrefix("file:///") {
                // keychain is a card
                cardFlag = false
            } else {
                cardFlag = true
            }
            
            let keychainSearch: [SecKeychain] = [keychain]
            
            let identitySearchDict: [String:AnyObject] = [
                kSecMatchSearchList as String: keychainSearch as AnyObject,
                kSecClass as String: kSecClassIdentity,
                kSecReturnAttributes as String: true as AnyObject,
                kSecReturnRef as String: true as AnyObject,
                kSecMatchLimit as String : kSecMatchLimitAll as AnyObject
            ]
            
            myErr = SecItemCopyMatching(identitySearchDict as CFDictionary, &searchReturn)
            
            if myErr != 0 {
                //NSLog("Error finding identities.")
                continue
            }
            
            let foundIdentites = searchReturn as! CFArray as Array
            
            for identity in foundIdentites {
                
                var certAuthority: String? = nil
                var certOrg: String? = nil
                
                //print(identity)
                // get the OIDs
                myErr = SecIdentityCopyCertificate(identity["v_Ref"] as! SecIdentity, &myCert)
                
                if myErr != 0 {
                    //NSLog("Error get SecCertificate from a SecIdentity")
                    continue
                }
                
                let myPrincipal = getPrincipal(identity: myCert!)
                
                let myOIDs : NSDictionary = SecCertificateCopyValues(myCert!, nil, nil)!
                
                //print(myOIDs.description)
                
                // get the CN from the SecCertificate
                var myCN: CFString? = nil
                myErr = SecCertificateCopyCommonName(myCert!, &myCN)
                
                // skip this loop if there's no label
                if identity["labl"] == nil {
                    continue
                }
                
                // get the CN
                let cnString = identity["labl"] as? String ?? "no label"
                
                let certPubKeyHashString = getPubKey(identity: identity as! Dictionary<AnyHashable, Any>)
                
                // check for token or not
                
                var tokenCheck = false
                
                if identity["agrp"] as? String == "com.apple.token" {
                    tokenCheck = true
                }
                
                // find CA
                
                if myOIDs["2.16.840.1.113741.2.1.1.1.5"] != nil {
                    guard let certAuthorityDict = myOIDs["2.16.840.1.113741.2.1.1.1.5"] as? [String:AnyObject] else { continue }
                    guard let certAuthorityDictValues = certAuthorityDict["value"] as? [[String:AnyObject]] else { continue }
                    for item in certAuthorityDictValues {
                        if item["label"] as! String == "2.5.4.3" {
                            certAuthority = ((item["value"] as? String) ?? nil)!
                        } else if item["label"] as! String == "2.5.4.11" {
                            certOrg = ((item["value"] as? String) ?? nil)!
                        }
                    }
                }
                
                // filter on OIDS
                
                if myOIDs["2.5.29.15"] != nil {
                    guard let usage = myOIDs["2.5.29.15"] as? NSDictionary else { continue }
                    guard let usageValue = usage["value"] as? Int else { continue }
                    
                    if ~~( abs(usageValue) & 128 ) { //&& defaults.bool(forKey: "ShowPIVAuthentication") {
                        friendlyName = "PIV Authentication"
                    } else if ~~( abs(usageValue) & 192 ) {
                        friendlyName = "PIV Signature"
                    } else if ~~( abs(usageValue) & 32 ) {
                        friendlyName = "Key Encipherment"
                    } else {
                        friendlyName = "Other Cert"
                    }
                    
                } else {
                    //print("NOT A KEY")
                    continue
                }
                
                if caMatch != "" && certAuthority != caMatch {
                    // skipping identity b/c of no CA match
                    continue
                }
                
                if caOrgMatch != "" && certOrg != caOrgMatch {
                    // skipping identity b/c of no CA Org match
                    continue
                }
                
                if filterOID != "" {
                    
                    guard let filterItem = myOIDs[filterOID] as? [String:AnyObject] else {
                        continue
                    }
                    
                    if String(describing: filterItem["value"]!) != filterTerm {
                        // no match
                        continue
                    }
                }
                
                //NSLog("Adding Identity: " + cnString + ":" + certPubKeyHashString)
                if cardFlag {
                    let myidList = identityList(cn: cnString, pubKeyHash: certPubKeyHashString, identity: identity["v_Ref"] as! SecIdentity, oids: myOIDs.description, token: tokenCheck, keychain: keychainType.card, keychainRef: keychain, principal: myPrincipal, friendlyName: friendlyName, ca: certAuthority, caOrg: certOrg)
                    idList.append(myidList)
                } else {
                    // check if we should show all identities:
                    
                }
                
            }
            
        }
        
        return idList
        
    }
    
    func getPrincipal(identity: SecCertificate) -> String {
        
        let principal = "none"
        
        // get all the OIDS
        
        guard let myOIDs : NSDictionary = SecCertificateCopyValues(identity, nil, nil) else { return principal }
        
        if debug { print(myOIDs) }
        
        // find which OID we want
        
        let matchValueHumanReadable =  matchValue
                
        switch matchValueHumanReadable {
        case "UPN": //print(myOIDs["2.5.29.17"] as Any)
            if myOIDs["2.5.29.17"] == nil {
                
                //print("***UPN WAS NIL***")
                return principal
            }
            
            guard let myUPNRaw = myOIDs["2.5.29.17"] as? NSDictionary else { return principal }
            guard let myUPNValues = myUPNRaw["value"] as? NSArray else { return principal }
            for item in myUPNValues {
                
                // cast to dictionary
                
                guard let itemDict = item as? NSDictionary else { return principal }
                guard let itemValue = itemDict["value"] as? String else { return principal }
                if itemValue.contains("@") {
                    return itemValue
                }
            }
            
        case "key":
            return principal
        default: return principal
        }
        
        // return the principal that we think we want
        
        return principal
    }
    
    func getPubKey(identity: Dictionary<AnyHashable, Any> ) -> String {
        // get the pubkey hash
        guard let certPubKeyHash = identity["pkhh"] as? NSData? else { return "" }
        guard let certPubKeyHashString = (certPubKeyHash?.description)?.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "").uppercased() else { return "" }
        return certPubKeyHashString
    }
}

